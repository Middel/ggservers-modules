from masterflix.core.bot import MasterFlix

from .antiphishing import AntiPhishing


async def setup(bot: MasterFlix):
    cog = AntiPhishing(bot)
    await bot.add_cog(cog)


__masterflix_end_user_data_statement__ = "This cog does not store any end user data."