## MasterFlix Modules

- [![Discord.py](https://img.shields.io/badge/discord-py-blue.svg)](https://github.com/Rapptz/discord.py)
- [![Python 3.11](https://img.shields.io/badge/python-v3.11-blue.svg)](https://www.python.org/downloads)

## External Modules

| Name:                | Version:     | Description:
|----------------------|--------------|----------------------------
| **AntiPhishing**     | *1.6.3.1*    | Screen for malicious links in servers.
| **Applications**     | *0.0.1*      | Create custom applications for your server. [ BETA ]
| **AutoRoom**         | *4.0.6*      | Automatically create temporarily voice channels for users.
| **Community**        | *1.0.2*      | Manage the Community Server from within Discord.
| **Counting**         | *1.7.0*      | Count from 1 to infinity!
| **ExtendedModlog**   | *2.12.5*     | Add various extra logging mechanisms.
| **Helpdesk**         | *2.3.0*      | Helps helpers & moderators do their job on the GGSERVERS Discord.
| **InviteBlocklist**  | *1.1.6*      | Prevent users from posting Discord Invite Links in specific channels.
| **MasTools**         | *3.0.2*      | This module has extra tools that are not available in core MasterFlix.
| **ServerStats**      | *1.8.0*      | Add various statistical related data.
| **Weather**          | *1.5.1*      | Show the current weather in specified locations.
| **Welcome**          | *2.5.3*      | Welcome users to the Discord.
| **Wikipedia**        | *3.1.0*      | Look up articles on Wikipedia.

## Credits

- **COMMUNITY SERVER:** https://www.community-server.nl | https://discord.gg/communitySMP
- **EVOLVED:** https://www.evolved-network.com | https://discord.gg/zeqxvPRxvX
- **GGSERVERS:** https://www.ggservers.com | https://discord.gg/ggservers