import discord

from masterflix.core import Config, checks, commands
from masterflix.core.utils.menus import menu, DEFAULT_CONTROLS


class HelpDesk(commands.Cog):
    """
        Helpdesk specifically designed for the GGSERVERS Discord.
    """

    __author__ = "_middel_"
    __version__ = "2.3.0"

    def __init__(self, bot):
        self.bot = bot

    def format_help_for_context(self, ctx: commands.Context):
        pre_processed = super().format_help_for_context(ctx)
        return f"{pre_processed}\n\nCog Version: {self.__version__}"

    async def masterflix_delete_data_for_user(self, **kwargs):
        """
        Nothing to delete
        """
        return

    def whitelist(ctx):
        return ctx.message.guild.id in [535452032258146324]

    @commands.group(autohelp=True, aliases=["gg"])
    @commands.bot_has_permissions(embed_links=True)
    @checks.mod_or_permissions(manage_messages=True)
    async def helpdesk(self, ctx):
        """
        GGSERVERS helpdesk commands.
        """

    @helpdesk.command(name="links")
    async def helpdesk_links(self, ctx):
        """
        Shows all the helpdesk commands.
        """
        # 20 links per page
        page1 = discord.Embed(
            title="__**Helpdesk Commands:**__",
            description="`!helpdesk backup`        - All information about backups.\n"
                        "`!helpdesk cancelservice` - Cancel your service with us.\n"
                        "`!helpdesk clearcache`    - Error 524 information.\n"
                        "`!helpdesk eta`           - ETA on tickets.\n"
                        "`!helpdesk ftp`           - Filezilla, WinSCP & Control Panel FTP usage and information.\n"
                        "`!helpdesk logs`          - How to send server logs.\n"
                        "`!helpdesk luckperms`     - Display all relevant Luckperms information.\n"
                        "`!helpdesk native`        - Native memory error information.\n"
                        "`!helpdesk op`            - OP yourself on your Minecraft server.\n"
                        "`!helpdesk openport`      - Finding an open port.\n"
                        "`!helpdesk paneldown`     - Control Panel is down information.\n"
                        "`!helpdesk pending`       - Service is pending.\n"
                        "`!helpdesk plans`         - Show all available plans.\n"
                        "`!helpdesk plugins`       - Plugin installation information.\n"
                        "`!helpdesk refund`        - Our refund policy information.\n"
                        "`!helpdesk resetpass`     - All password related guides.\n"
                        "`!helpdesk ticket`        - Support ticket instructions.\n"
                        "`!helpdesk ticking`       - Ticking entity information.\n"
                        "`!helpdesk upgrade`       - Information about upgrades.\n"
                        "`!helpdesk watchdog`      - Disable watchdog.\n",
            color=discord.Colour.blue()
        )

        page1.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        page1.set_footer(text="Helpdesk v2.3.0 | Page 1/2")

        page2 = discord.Embed(
            title="__**Helpdesk Commands:**__",
            description="`!helpdesk whatpremium` - Differences between premium and standard plans.\n"
                        "`!helpdesk worldmgmt`   - All world management guides.",
            color=discord.Colour.blue()
        )

        page2.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        page2.set_footer(text="Helpdesk v2.3.0 | Page 2/2")

        pages = [page1, page2]

        await menu(ctx, pages, DEFAULT_CONTROLS)

    @helpdesk.command(name="backup")
    async def helpdesk_backup(self, ctx):
        """
        All information about backups.
        """
        backup = discord.Embed(
            title="__**All about backups:**__",
            description="[Access Control Panel](https://panel.ggservers.com/)",
            color=discord.Colour.blue()
        )

        fields = [
            ("Difference between autosaves & backups:", "[Help article](https://ggservers.com/knowledgebase/article/whats-the-difference-between-an-autosave-and-a-backup/)", False),
            ("Make a backup + download it:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-make-a-backup-and-download-it/)", False),
            ("Creating automatic backup task:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-create-automatic-backups/)", False),
            ("Restore backup:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-restore-a-backup/)", False)
        ]

        backup.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        backup.set_thumbnail(url="https://i.imgur.com/pFjrQbQ.png")

        for name, value, inline in fields:
            backup.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=backup)

    @helpdesk.command(name="cancelservice")
    async def helpdesk_cancelservice(self, ctx):
        """
        Cancel your service with us. 
        """
        cancelservice = discord.Embed(
            title="__**Cancel Your Service:**__",
            description="These articles will show you how to cancel your service with us.\n"
                        "[Cancel Service article](https://ggservers.com/knowledgebase/article/how-to-cancel-your-service/)\n"
                        "[Cancel PayPal Subscription article](https://ggservers.com/knowledgebase/article/how-to-cancel-your-paypal-subscription/)",
            color=discord.Colour.blue()
        )

        cancelservice.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=cancelservice)

    @helpdesk.command(name="clearcache")
    async def helpdesk_clearcache(self, ctx):
        """
        Error 524 information.
        """
        clearcache = discord.Embed(
            title="__**Clear Browser Cache/Cookies**__",
            description="Please go to a webpage, press the padlock on the address bar, click cookies, then remove them all, click done, then refresh the page, this should fix the problem. If these instructions don't work for you, please clear the cookies / cache of your browser.",
            color=discord.Colour.blue()
        )

        clearcache.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=clearcache)

    @helpdesk.command(name="eta")
    async def helpdesk_eta(self, ctx):
        """
        ETA on tickets. 
        """
        eta = discord.Embed(
            title="__**Ticket Response Time:**__",
            description="Staff do tickets in order of them being received. You will receive a response once it has been resolved. Your patience is appreciated!",
            color=discord.Colour.blue()
        )

        eta.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=eta)

    @helpdesk.command(name="ftp")
    async def helpdesk_ftp(self, ctx):
        """
        Filezilla, WinSCP and Control Panel FTP usage and information.
        """
        ftp = discord.Embed(
            title="__**Filezilla | WinSCP | Control Panel FTP:**__",
            description="FTP to access your server files",
            color=discord.Colour.blue()
        )

        fields = [
            ("Filezilla Client:", "[Download here](https://filezilla-project.org/download.php?type=client)", True),
            ("WinSCP Client:", "[Download here](https://winscp.net/eng/download.php)", True),
            ("Control Panel FTP:", "[Access Control Panel](https://panel.ggservers.com/)", True),
            ("Filezilla usage:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-connect-to-your-server-using-filezilla/)", True),
            ("WinSCP usage:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-connect-to-your-server-using-winscp/)", True),
            ("Control Panel FTP usage:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-access-your-minecraft-server-files-via-ftp/)", True)
        ]

        ftp.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        ftp.set_thumbnail(url="https://imgur.com/LYmUGdT.png")

        for name, value, inline in fields:
            ftp.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=ftp)

    @helpdesk.command(name="logs")
    async def helpdesk_logs(self, ctx):
        """
        Logs in chat.
        """
        logs = discord.Embed(
            title="__**Console/Crash Logs:**__", 
            description="**Step 1**. Go to your console in [Access Control Panel](https://panel.ggservers.com/).\n"
                        "**Step 2**. Select the entire console.\n"
                        "**Step 3**. Copy it.\n"
                        "**Step 4**. Paste the logs [Click here](https://givemelogs.com/).\n"
                        "**Step 5**. Click on submit.\n"
                        "**Step 6**. Copy the web address URL from the address bar.\n"
                        "**Step 7**. Paste it in <#1019844003673415773>.\n",
            color=discord.Colour.blue()
        )

        logs.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=logs)

    @helpdesk.command(name="luckperms")
    async def helpdesk_luckperms(self, ctx):
        """
        Display all relevant luckperms information.
        """
        luckperms = discord.Embed(
            title="__**Luckperms Setup:**__",
            description="[Download here](https://luckperms.net/download)",
            color=discord.Colour.blue()
        )

        fields = [
            ("Change server type | Bukkit, Spigot, Paper:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-change-your-server-type-and-version/)", False),
            ("Installation of Luckperms:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-set-up-and-use-luckperms/)", False),
            ("OP yourself:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-op-yourself-on-your-minecraft-server/)", False),
            ("Luckperms usage:", "[LuckPerms Wiki](https://luckperms.net/wiki/Home)", False)
        ]

        luckperms.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        luckperms.set_thumbnail(url="https://storage.crisp.chat/users/helpdesk/website/ba33bb39ceb6d800/lp_4o1qyq.png")

        for name, value, inline in fields:
            luckperms.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=luckperms)

    @helpdesk.command(name="native")
    async def helpdesk_native(self, ctx):
        """
        Native memory error information.
        """
        native = discord.Embed(
            title="__**Native Error:**__",
            description="## There is insufficient memory for the Java Runtime Environment to continue.",
            color=discord.Colour.blue()
        )

        fields = [
            ("Solution:", "Open a ticket to the Node Transfer department", False),
            ("Open Ticket:", "[Billing area](https://ggservers.com/billing/submitticket.php) | Login to your GGServers Account", False)
        ]

        native.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        for name, value, inline in fields:
            native.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=native)

    @helpdesk.command(name="op")
    async def helpdesk_op(self, ctx):
        """
        How to OP yourself.
        """
        op = discord.Embed(
            title="__**How to: OP yourself on your Minecraft server!**__",
            description="[Help article](https://ggservers.com/knowledgebase/article/how-to-op-yourself-on-your-minecraft-server/)",
            color=discord.Colour.blue()
        )

        op.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        op.set_thumbnail(url="https://imgur.com/oxqv2yz.png")

        await ctx.send(embed=op)

    @helpdesk.command(name="openport")
    async def helpdesk_openport(self, ctx):
        """
        Open port for plugins. 
        """
        openport = discord.Embed(
            title="__**Finding an open port for plugins:**__",
            description="This guide will show you how to find an open port for any plugin that needs an extra one.\n"
                        "[Help article](https://ggservers.com/knowledgebase/article/how-to-find-an-additional-port-for-your-pluginmod/)",
            color=discord.Colour.blue()
        )

        openport.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=openport)

    @helpdesk.command(name="paneldown")
    async def helpdesk_paneldown(self, ctx):
        """
        Control Panel Down.
        """
        paneldown = discord.Embed(
            title="__**Control Panel Down | Error 500**__",
            description="Staff members are aware of the issue right now. Thank you for reporting it. They're working on a fix as we speak. Appreciate the patience, and our sincere apologies for the inconvenience.",
            color=discord.Colour.blue()
        )

        paneldown.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=paneldown)

    @helpdesk.command(name="pending")
    async def helpdesk_pending(self, ctx):
        """
        Pending service.
        """
        pending = discord.Embed(
            title="__**Pending Service**__",
            description="If your server is pending that means that we're still setting up your new server. Most of the time, it happens instantly. In other instances though, we have to do some setup.",
            color=discord.Colour.blue()
        )

        fields = [("Why is my service pending?", "[Help article](https://ggservers.com/knowledgebase/article/pending-service/)", False)]

        pending.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        pending.set_thumbnail(url="https://imgur.com/up4tsKq.png")

        for name, value, inline in fields:
            pending.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=pending)

    @helpdesk.command(name="plans")
    async def helpdesk_plans(self, ctx):
        """
        All our plans available for Minecraft Hosting.
        """
        stone = discord.Embed(
            title="Stone Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.light_grey()
        )

        fields = [
            ("Price:", "$3/mo", True),
            ("Ram:", "1GB", True),
            ("Slots:", "12 Total", True)
        ]

        stone.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        stone.set_thumbnail(url="https://ggservers.com/images/plan-stone.webp")
        stone.set_footer(text="Page 1/10")

        for name, value, inline in fields:
            stone.add_field(name=name, value=value, inline=inline)

        coal = discord.Embed(
            title="Coal Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.darker_grey()
        )

        fields = [
            ("Price:", "$6/mo", True),
            ("Ram:", "2GB", True),
            ("Slots:", "24 Total", True)
        ]

        coal.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        coal.set_thumbnail(url="https://ggservers.com/images/plan-coal.webp")
        coal.set_footer(text="Page 2/10")

        for name, value, inline in fields:
            coal.add_field(name=name, value=value, inline=inline)

        iron = discord.Embed(
            title="Iron Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.dark_orange()
        )

        fields = [
            ("Price:", "$9/mo", True),
            ("Ram:", "3GB", True),
            ("Slots:", "36 Total", True)
        ]

        iron.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        iron.set_thumbnail(url="https://ggservers.com/images/plan-iron.webp")
        iron.set_footer(text="Page 3/10")

        for name, value, inline in fields:
            iron.add_field(name=name, value=value, inline=inline)

        gold = discord.Embed(
            title="Gold Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.gold()
        )

        fields = [
            ("Price:", "$12/mo", True),
            ("Ram:", "4GB", True),
            ("Slots:", "48 Total", True)
        ]

        gold.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        gold.set_thumbnail(url="https://ggservers.com/images/plan-gold.webp")
        gold.set_footer(text="Page 4/10")

        for name, value, inline in fields:
            gold.add_field(name=name, value=value, inline=inline)

        lapis = discord.Embed(
            title="Lapis Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.dark_blue()
        )

        fields = [
            ("Price:", "$15/mo", True),
            ("Ram:", "5GB", True),
            ("Slots:", "60 Total", True)
        ]

        lapis.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        lapis.set_thumbnail(url="https://ggservers.com/images/plan-lapis.webp")
        lapis.set_footer(text="Page 5/10")

        for name, value, inline in fields:
            lapis.add_field(name=name, value=value, inline=inline)

        redstone = discord.Embed(
            title="Redstone Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.red()
        )

        fields = [
            ("Price:", "$18/mo", True),
            ("Ram:", "6GB", True),
            ("Slots:", "72 Total", True)
        ]

        redstone.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        redstone.set_thumbnail(url="https://ggservers.com/images/plan-redstone.webp")
        redstone.set_footer(text="Page 6/10")

        for name, value, inline in fields:
            redstone.add_field(name=name, value=value, inline=inline)

        diamond = discord.Embed(
            title="Diamond Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.teal()
        )

        fields = [
            ("Price:", "$24/mo", True),
            ("Ram:", "8GB", True),
            ("Slots:", "96 Total", True)
        ]

        diamond.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        diamond.set_thumbnail(url="https://ggservers.com/images/plan-diamond.webp")
        diamond.set_footer(text="Page 7/10")

        for name, value, inline in fields:
            diamond.add_field(name=name, value=value, inline=inline)

        emerald = discord.Embed(
            title="Emerald Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.green()
        )

        fields = [
            ("Price:", "$36/mo", True),
            ("Ram:", "12GB", True),
            ("Slots:", "144 Total", True)
        ]

        emerald.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        emerald.set_thumbnail(url="https://ggservers.com/images/plan-emerald.webp")
        emerald.set_footer(text="Page 8/10")

        for name, value, inline in fields:
            emerald.add_field(name=name, value=value, inline=inline)

        amethyst = discord.Embed(
            title="Amethyst Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.purple()
        )

        fields = [
            ("Price:", "$48/mo", True),
            ("Ram:", "16GB", True),
            ("Slots:", "192 Total", True)
        ]

        amethyst.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        amethyst.set_thumbnail(url="https://ggservers.com/images/plan-amethyst.webp")
        amethyst.set_footer(text="Page 9/10")

        for name, value, inline in fields:
            amethyst.add_field(name=name, value=value, inline=inline)

        beacon = discord.Embed(
            title="Beacon Plan",
            description="[Order Plan](https://ggservers.com/minecrafthosting)",
            color=discord.Colour.blue()
        )

        fields = [
            ("Price:", "$96/mo", True),
            ("Ram:", "32GB", True),
            ("Slots:", "384 Total", True)
        ]

        beacon.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        beacon.set_thumbnail(url="https://ggservers.com/images/plan-beacon.webp")
        beacon.set_footer(text="Page 10/10")

        for name, value, inline in fields:
            beacon.add_field(name=name, value=value, inline=inline)

        pages = [stone, coal, iron, gold, lapis, redstone, diamond, emerald, amethyst, beacon]

        await menu(ctx, pages, DEFAULT_CONTROLS)

    @helpdesk.command(name="plugins")
    async def helpdesk_plugins(self, ctx):
        """
        Plugin Installation Information.
        """
        plugins = discord.Embed(
            title="__**Plugins Installation:**__",
            description="First, you have to be sure that your server is running on Spigot, CraftBukkit or PaperSpigot! Vanilla can't run plugins.",
            color=discord.Colour.blue()
        )

        fields = [
            ("Change server type/version:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-change-your-server-type-and-version/)", False),
            ("Installation:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-install-plugins-on-your-server/)", False),
            ("Recommended plugins:", "[Help article](https://ggservers.com/knowledgebase/article/recommended-minecraft-plugins/)", False)
        ]

        plugins.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        for name, value, inline in fields:
            plugins.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=plugins)

    @helpdesk.command(name="refund")
    async def helpdesk_refund(self, ctx):
        """
        Our refund policy information.
        """
        refund = discord.Embed(
            title="__**Refund**__",
            description="How to get a refund on your service",
            color=discord.Colour.blue()
        )

        fields = [
            ("Reimbursement Policy:", "We can only offer a refund for a service if the request was made within 24 hours of purchase.", False),
            ("Ticket | Billing Department:", "[Billing area](https://ggservers.com/billing/submitticket.php) | Login to your GGServers Account", False)
        ]

        refund.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        for name, value, inline in fields:
            refund.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=refund)

    @helpdesk.command(name="resetpass")
    async def helpdesk_resetpass(self, ctx):
        """
        All password reset guides.
        """
        resetpass = discord.Embed(
            title="__**Control Panel/Billing Password Reset:**__",
            description="These articles help you to reset your passwords.",
            color=discord.Colour.blue()
        )

        fields = [
            ("Control Panel Password:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-reset-your-multicraft-password/)", False),
            ("Billing Area Password:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-reset-your-billing-area-password/)", False)
        ]

        resetpass.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        for name, value, inline in fields:
            resetpass.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=resetpass)
  
    @helpdesk.command(name="ticket")
    async def helpdesk_ticket(self, ctx):
        """
        How to open a support ticket.
        """
        ticket = discord.Embed(
            title="__**Support ticket instructions:**__",
            description="[Help article](https://ggservers.com/knowledgebase/article/how-to-open-a-support-ticket/)",
            color=discord.Colour.blue()
        )
        
        ticket.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        ticket.set_thumbnail(url="https://imgur.com/BNWZk4S.png")
        
        await ctx.send(embed=ticket)

    @helpdesk.command(name="ticking")
    async def helpdesk_ticking(self, ctx):
        """
        Ticking entity information. 
        """
        ticking = discord.Embed(
            title="__**Ticking Entity Error:**__",
            description="A Ticking Entity is a mob/creature/NPC that had become corrupt in your world.\n"
                        "[Help article](https://ggservers.com/knowledgebase/article/what-is-a-ticking-entity-error-and-how-to-fix-it/)",
            color=discord.Colour.blue()
        )

        ticking.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=ticking)

    @helpdesk.command(name="upgrade")
    async def helpdesk_upgrade(self, ctx):
        """
        Information about upgrades.
        """
        upgrade = discord.Embed(
            title="__**Upgrade/Downgrade Service:**__",
            description="[Help article](https://ggservers.com/knowledgebase/article/how-to-upgrade-or-downgrade-your-service/)",
            color=discord.Colour.blue()
        )

        fields = [
            ("Ram:", "[Billing Area](http://ggservers.com/billing)", True),
            ("Standard | Premium:", "You will need to open a ticket to billing department requesting it.", True),
            ("Ticket - Login to GGServers Account:", "[Click Here](https://ggservers.com/billing/submitticket.php)", False)
        ]

        upgrade.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        upgrade.set_thumbnail(url="https://imgur.com/fwWcU2N.png")

        for name, value, inline in fields:
            upgrade.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=upgrade)

    @helpdesk.command(name="watchdog")
    async def helpdesk_watchdog(self, ctx):
        """
        Disable watchdog. 
        """
        watchdog = discord.Embed(
            title="__**Watchdog Error:**__",
            description="**Step 1** - Stop the server.\n"
                        "**Step 2** - Go to Server Properties.\n"
                        "**Step 3** - Change the Max tick time to -10000.\n"
                        "**Step 4** - Save it.\n"
                        "**Step 5** - Start the server again.",
            color=discord.Colour.blue()
        )

        watchdog.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        await ctx.send(embed=watchdog)

    @helpdesk.command(name="whatpremium")
    async def helpdesk_whatpremium(self, ctx):
        """
        Differences between premium and standard plans.
        """
        premium = discord.Embed(
            title="__**PREMIUM**__",
            description="[Help article](https://ggservers.com/knowledgebase/article/the-difference-between-standard-premium/)", 
            color=discord.Colour.blue()
        )

        fields = [
            ("Price/mo:", "$6/GB", False),
            ("Locations:", "Canada | UK | France | Germany | Finland | Australia | Singapore | Virginia | Oregon", False),
            ("CPU:", "4.4-5 GHz (High Priority)", True),
            ("RAM:", "DDR4 2400 MHz", True),
            ("Storage:", "NVMe (2500+ MB/s r/w)", True),
            ("Database:", "Free", False),
            ("Unlimited Slots:", "Free", True),
            ("Subdomain:", "Free", True)
        ]

        premium.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        premium.set_thumbnail(url="https://imgur.com/Wzg84ui.png")

        for name, value, inline in fields:
            premium.add_field(name=name, value=value, inline=inline)
     
        standard = discord.Embed(
            title="__**STANDARD**__",
            description="[Help article](https://ggservers.com/knowledgebase/article/the-difference-between-standard-premium/)",
            color=discord.Colour.blue()
        )

        fields = [
            ("Price/mo:", "$3/GB", False),
            ("Locations:", "Canada | France", False),
            ("CPU:", "3.2-4 GHz", True),
            ("RAM:", "DDR4 2133 MHz", True),
            ("Storage:", "SOFT RAID 1 (400 MB/s r/w)", True),
            ("Database:", "$2/mo Addon", False),
            ("Unlimited Slots:", "$2 Addon", True),
            ("Subdomain:", "Free", True)
        ]

        standard.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)
        standard.set_thumbnail(url="https://imgur.com/Wzg84ui.png")

        for name, value, inline in fields:
            standard.add_field(name=name, value=value, inline=inline)

        pages = [premium, standard]

        await menu(ctx, pages, DEFAULT_CONTROLS)

    @helpdesk.command(name="worldmgmt")
    async def helpdesk_worldmgmt(self, ctx):
        """
        All world management guides.
        """
        worldmgmt = discord.Embed(
            title="__**World Management:**__",
            description="These articles are for managing your Minecraft worlds.",
            color=discord.Colour.blue()
        )

        fields = [
            ("World Border:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-set-up-and-use-a-world-border/)", False),
            ("WorldBorder Plugin:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-set-up-and-use-worldborder-plugin/)", False),
            ("View & Change World Seed:", "[Help article](https://ggservers.com/knowledgebase/article/how-to-view-and-change-your-world-seed/)", False)
        ]

        worldmgmt.set_author(name="GGSERVERS", icon_url=ctx.guild.icon.url)

        for name, value, inline in fields:
            worldmgmt.add_field(name=name, value=value, inline=inline)

        await ctx.send(embed=worldmgmt)