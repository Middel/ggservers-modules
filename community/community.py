# Importing necessary libraries.
import io
import re
import base64
import logging
import discord
from typing import Dict, Tuple
from discord import Embed
from masterflix.core import Config, checks, commands
from masterflix.core.bot import MasterFlix
from masterflix.core.utils.chat_formatting import pagify
from masterflix.core.utils.menus import DEFAULT_CONTROLS, menu
from mcstatus import JavaServer
from aiomcrcon import Client
from aiomcrcon.errors import IncorrectPasswordError, RCONConnectionError

# Set up a logger to log information for debugging or tracking errors.
log = logging.getLogger("masterflix.middel.community")

# Define a regular expression pattern to validate Minecraft usernames.
re_username = re.compile(r"^.?\w{3,30}$")


class Community(commands.Cog):
    """Beheer de Community Server vanuit de Discord-server en controleer de serverstatus en online spelers zonder de game te openen."""
    __version__ = "1.0.2"

    def __init__(self, bot: MasterFlix):
        """Initialiseer de Community Cog met de botinstantie en stel de configuratie in."""
        super().__init__()
        self.bot = bot
        self.clients: Dict[int, Client] = {}
        self.config = Config.get_conf(self, identifier=110320200153)
        default_guild = {
            "players": {},
            "host": "localhost",
            "port": 25565,
            "rcon_port": 25575,
            "password": "",
            "players_to_delete": [],
        }
        self.config.register_guild(**default_guild)

    async def initialize(self):
        pass

    async def cog_load(self):
        all_data = await self.config.all_guilds()
        for guild_id, data in all_data.items():
            if data["password"]:
                self.clients[guild_id] = Client(data["host"], data["rcon_port"], data["password"])
            # Old version.
            updated = False
            for user_id, player in list(data["players"].items()):
                if isinstance(player, dict):
                    del data["players"][user_id]
                    data["players"][user_id] = player["name"]
                    updated = True
            if updated:
                await self.config.guild_from_id(guild_id).players.set(data["players"])

    async def cog_unload(self):
        for client in self.clients.values():
            await client.close()

    # Asynchronous method to delete player data for a specific user across all guilds.
    async def masterflix_delete_data_for_user(self, requester: str, user_id: int):
        all_data = await self.config.all_guilds()
        for guild_id in all_data:
            if str(user_id) in all_data[guild_id]["players"]:
                del all_data[guild_id]["players"][str(user_id)]
                await self.config.guild_from_id(guild_id).players.set(all_data[guild_id]["players"])

    # Asynchronous method to run a command on the Community Server using RCON.
    async def run_minecraft_command(self, guild: discord.Guild, command: str) -> Tuple[bool, str]:
        if guild.id not in self.clients:
            return False, "Stel eerst de module in."
        try:
            async with self.clients[guild.id] as client:
                resp = await client.send_cmd(command, 10)
            return True, resp[0]
        except (RCONConnectionError, TimeoutError) as error:
            return False, error or "Kon geen verbinding maken met de Community Server."
        except IncorrectPasswordError:
            return False, "Onjuist RCON-wachtwoord."
        except Exception as error:  # Catch any other exceptions that occur.
            log.exception("Bezig met het uitvoeren van het commando")
            return False, f"{type(error).__name__}: {error}"

    # Listener that triggers when a member leaves the Discord server.
    @commands.Cog.listener()
    async def on_member_remove(self, member: discord.Member):
        """Verwijder een speler van de whitelist wanneer ze de Discord verlaten."""
        players = await self.config.guild(member.guild).players()
        if str(member.id) in players:
            success, _ = await self.run_minecraft_command(member.guild, f"whitelist remove {players[str(member.id)]}")
            if not success:
                async with self.config.guild(member.guild).players_to_delete() as players_to_delete:
                    players_to_delete.append(players[str(member.id)])
            async with self.config.guild(member.guild).players() as cur_players:
                del cur_players[str(member.id)]

    # Asynchronous method to remove orphaned players from the Community Server whitelist.
    async def delete_orphan_players(self, guild: discord.Guild):
        players_to_delete = await self.config.guild(guild).players_to_delete()
        if not players_to_delete:
            return
        for player in players_to_delete:
            success, _ = await self.run_minecraft_command(guild, f"whitelist remove {player}")
            if not success:
                return
            async with self.config.guild(guild).players_to_delete() as cur_players_to_delete:
                cur_players_to_delete.remove(player)
        await self.run_minecraft_command(guild, f"whitelist reload")


    @commands.group()
    async def community(self, ctx):
        """Community Server-commando's."""
        pass

    # Command to set up the Community module, only accessible by admins or users with the "Manage Server" permission.
    @checks.admin_or_permissions(manage_guild=True)
    @community.command()
    async def setup(self, ctx: commands.Context, host: str, port: int, rcon_port: int, *, password: str):
        """Stel de module in.

        `host`: Het IP adres of de URL van de Community Server.
        `port`: De normale serverpoort (standaard is 25565).
        `rcon_port`: De RCON-poort van de server (standaard is 25575).
        `password`: Het RCON-wachtwoord.
        RCON moet ingeschakeld zijn en correct ingesteld worden in het bestand 'server.properties'.
        """
        await ctx.message.delete()
        await self.config.guild(ctx.guild).host.set(host)
        await self.config.guild(ctx.guild).port.set(port)
        await self.config.guild(ctx.guild).rcon_port.set(rcon_port)
        await self.config.guild(ctx.guild).password.set(password)
        if self.clients.get(ctx.guild.id, None):
            await self.clients[ctx.guild.id].close()
        self.clients[ctx.guild.id] = Client(host, rcon_port, password)
        try:
            async with self.clients[ctx.guild.id] as client:
                await client.send_cmd("help")
        except (RCONConnectionError, TimeoutError) as error:
            await ctx.send((error or "Kon geen verbinding maken met de Community Server.") +
                           "\nZorg ervoor dat de Community Server online is en dat de waarden correct zijn, en dat de RCON-poort open is voor het publiek.")
        except IncorrectPasswordError:
            await ctx.send("Onjuist RCON-wachtwoord.")
        except Exception as error:  # Log unexpected errors and provide feedback to the user.
            log.exception("Bezig met het uitvoeren van het commando")
            if f"{error}" == "Uitpakken vereist een buffer van 4 bytes":
                await ctx.send("Kon geen verbinding maken met de Community Server. Mogelijk heb je de poort en RCON-poort omgedraaid.")
            else:
                await ctx.send(f"{type(error).__name__}: {error}")
        else:
            await ctx.send("✅ RCON Server informatie opgeslagen!")

    # Command to display information about the Community Server.
    @commands.bot_has_permissions(embed_links=True)
    @community.command()
    async def status(self, ctx: commands.Context):
        """Toon informatie over de Community Server."""
        host = await self.config.guild(ctx.guild).host()
        port = await self.config.guild(ctx.guild).port()
        ip = f"{host}:{port}"
        try:
            server = await JavaServer.async_lookup(ip)
            status = await server.async_status() if server else None
        except (ConnectionError, TimeoutError):
            status = None
        except Exception as error:  # Log unexpected errors and provide feedback to the user.
            if f"{error}" == "De socket reageerde niet met enige informatie!":
                return await ctx.send("🟡 De Community Server is mogelijk in slaapmodus! Je kunt proberen in te loggen om hem weer op te starten.")
            log.exception(f"Bezig met het ophalen van de status voor {ip}")
            return await ctx.send(f"Er is een fout opgetreden. {error}")

        if not status:
            embed = discord.Embed(title=f"Community Server", color=0xFF0000)
            embed.add_field(name="IP", value=ip)
            embed.add_field(name="Status", value="🔴 Offline")
            file = None
        else:
            embed = discord.Embed(title=f"Community Server", color=0x00FF00)
            if status.motd:
                embed.add_field(name="Informatie", value=status.motd.to_plain(), inline=False)
            embed.add_field(name="IP", value=ip)
            embed.add_field(name="Versie", value=status.version.name)
            embed.add_field(name="Status", value="🟢 Online")
            embed.add_field(name=f"Spelers ({status.players.online}/{status.players.max})",
                            value="\n" + ", ".join([p.name for p in status.players.sample]) if status.players.online else "*Geen*")
            if status.icon:
                b = io.BytesIO(base64.b64decode(status.icon.removeprefix("data:image/png;base64,")))
                filename = "server.png"
                file = discord.File(b, filename=filename)
                embed.set_thumbnail(url=f"attachment://{filename}")
            else:
                file = None

        await ctx.send(embed=embed, file=file)

    # Command to add yourself to the Community Server whitelist.
    @checks.mod_or_permissions(manage_messages=True)
    @community.command()
    async def join(self, ctx: commands.Context, name: str):
        """Voeg jezelf toe aan de whitelist. Je wordt verwijderd wanneer je de Discord verlaat."""
        if not re_username.match(name):
            return await ctx.send(f"Ongeldige gebruikersnaam.")

        players = await self.config.guild(ctx.guild).players()
        if str(ctx.author.id) in players:
            return await ctx.send(f"Je staat al op de whitelist.\nJe kunt jezelf verwijderen met {ctx.clean_prefix}community leave")

        success, msg = await self.run_minecraft_command(ctx.guild, f"whitelist add {name}")
        if "Die speler bestaat niet" in msg:
            return await ctx.send("Onbekende speler. Probeer in te loggen op de Community Server zodat deze je herkent.")
        await ctx.send(msg)
        if not success:
            return

        async with self.config.guild(ctx.guild).players() as cur_players:
            cur_players[str(ctx.author.id)] = name

        await self.delete_orphan_players(ctx.guild)

        success, msg = await self.run_minecraft_command(ctx.guild, "whitelist reload")
        await ctx.send(msg)

    # Command to remove yourself from the Community Server whitelist.
    @checks.mod_or_permissions(manage_messages=True)
    @community.command()
    async def leave(self, ctx: commands.Context):
        """Verwijder jezelf van de whitelist."""
        players = await self.config.guild(ctx.guild).players()

        if str(ctx.author.id) not in players:
            return await ctx.send("Je bent niet geregistreerd op de Community Server via Discord.")

        async with self.config.guild(ctx.guild).players() as cur_players:
            del cur_players[str(ctx.author.id)]

        success, msg = await self.run_minecraft_command(ctx.guild, f"whitelist remove {players[str(ctx.author.id)]}")
        await ctx.send(msg)
        if not success:
            async with self.config.guild(ctx.author.guild).players_to_delete() as players_to_delete:
                players_to_delete.append(players[str(ctx.author.id)])
            return

        await self.delete_orphan_players(ctx.guild)

        success, msg = await self.run_minecraft_command(ctx.guild, "whitelist reload")
        await ctx.send(msg)

    # Command to add a player to the Community Server whitelist.
    @checks.mod_or_permissions(manage_messages=True)
    @community.command()
    async def add(self, ctx: commands.Context, name: str):
        """Voeg een speler toe aan de whitelist met hun Minecraft-gebruikersnaam. Ze worden niet automatisch verwijderd wanneer ze de Discord verlaten."""
        if not re_username.match(name):
            return await ctx.send(f"Ongeldige gebruikersnaam.")

        success, msg = await self.run_minecraft_command(ctx.guild, f"whitelist add {name}")
        await ctx.send(msg)
        if not success:
            return

        await self.delete_orphan_players(ctx.guild)

        success, msg = await self.run_minecraft_command(ctx.guild, "whitelist reload")
        await ctx.send(msg)

    # Command to remove a player from the Community Server whitelist.
    @checks.mod_or_permissions(manage_messages=True)
    @community.command()
    async def remove(self, ctx: commands.Context, name: str):
        """Verwijder een speler van de whitelist met hun Minecraft-gebruikersnaam."""
        if not re_username.match(name):
            return await ctx.send(f"Ongeldige gebruikersnaam.")

        success, msg = await self.run_minecraft_command(ctx.guild, f"whitelist remove {name}")
        await ctx.send(msg)
        if not success:
            return

        await self.delete_orphan_players(ctx.guild)

        success, msg = await self.run_minecraft_command(ctx.guild, "whitelist reload")
        await ctx.send(msg)

    # Command to view the whitelist of the Community Server.
    @checks.mod_or_permissions(manage_messages=True)
    @commands.bot_has_permissions(embed_links=True)
    @community.command()
    async def whitelist(self, ctx: commands.Context):
        """Bekijk wie er op de whitelist staat van de Community Server."""

        success, msg = await self.run_minecraft_command(ctx.guild, "whitelist list")
        await ctx.send(msg if len(msg) <= 2000 else msg[:1997] + "...")

        if success:
            await self.delete_orphan_players(ctx.guild)

        players = await self.config.guild(ctx.guild).players()
        if len(players) == 0:
            await ctx.send("Niemand heeft zichzelf via Discord op de whitelist gezet.")
            return

        outstr = []
        for user_id, player in players.items():
            outstr.append(f"<@{user_id}> | {player}\n")

        pages = list(pagify("\n".join(outstr), page_length=1024))
        rendered = []
        for page in pages:
            emb = Embed(title="Op de whitelist via Discord:", description=page, color=0xFFA500)
            rendered.append(emb)

        await menu(ctx, rendered, controls=DEFAULT_CONTROLS, timeout=60.0)

    # Command to execute a console command from within the Discord server.
    @checks.admin_or_permissions(manage_guild=True)
    @community.command()
    async def command(self, ctx: commands.Context, *, command: str):
        """Voer een commando uit op de Community Server. Er wordt geen validatie uitgevoerd."""
        if len(command) > 1440:
            return await ctx.send("Commando te lang!")
        success, resp = await self.run_minecraft_command(ctx.guild, command)
        await ctx.send(resp or "✅")
        if success:
            await self.delete_orphan_players(ctx.guild)